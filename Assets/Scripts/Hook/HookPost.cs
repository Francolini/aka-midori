﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HookPost : MonoBehaviour
{
    private float playerPosY;

    private Collider2D platformCol;
    private PlayerScript playerScript;

    public GameObject player;
    public GameObject hookPoint;
    public GameObject platform;
    public GameObject visual;

    // Start is called before the first frame update
    void Start()
    {
        playerScript = player.GetComponent<PlayerScript>();
        platformCol = platform.GetComponent<Collider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        playerPosY = player.GetComponent<Transform>().position.y;

        if(playerPosY > platform.transform.position.y + 1.5)
        {
            platform.layer = 8;
            platformCol.isTrigger = false;

        } else
        {
            platformCol.isTrigger = true;
            platform.layer = 0;
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Player") && !playerScript.bugFixer)
        {
            visual.SetActive(true);
            playerScript.canHook = true;
            playerScript.hookPoint = hookPoint;
        }
    }

    void OnTriggerStay2D(Collider2D col)
    {
        if (col.CompareTag("Player") && !playerScript.bugFixer)
        {
            visual.SetActive(true);
            playerScript.canHook = true;
            playerScript.hookPoint = hookPoint;
        }
    }

    void OnTriggerExit2D(Collider2D col)
    {
        if (col.CompareTag("Player"))
        {
            visual.SetActive(false);

            if (!playerScript.bugFixer)
            {
                playerScript.canHook = false;
            }
        }
    }
}