﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    private Image fundido, tituloImage, restartImage, quitImage;
    private Button restartButton, quitButton;
    private Text restartText, quitText;

    private float opacidad, opacidadLogo, opacidadBotones;

    public GameObject titulo, restartButtonObject, restartImageObject, quitButtonObject, quitImageObject;

    public bool gameFinished;

    // Start is called before the first frame update
    void Start()
    {
        tituloImage = titulo.GetComponent<Image>();
        fundido = GetComponent<Image>();
        restartImage = restartButtonObject.GetComponent<Image>();
        quitImage = quitButtonObject.GetComponent<Image>();

        restartText = restartImageObject.GetComponent<Text>();
        quitText = quitImageObject.GetComponent<Text>();

        quitButton = quitButtonObject.GetComponent<Button>();
        restartButton = restartButtonObject.GetComponent<Button>();

        opacidad = 0;
        opacidadLogo = 0;
        opacidadBotones = 0;

        fundido.color = new Color(fundido.color.r, fundido.color.g, fundido.color.b, opacidad);
        tituloImage.color = new Color(tituloImage.color.r, tituloImage.color.g, tituloImage.color.b, opacidad);
    }

    // Update is called once per frame
    void Update()
    {
        if (gameFinished)
        {
            if (opacidad <= 1)
            {
                opacidad += Time.deltaTime / 3;
                fundido.color = new Color(fundido.color.r, fundido.color.g, fundido.color.b, opacidad);
            }
            else
            {
                opacidadLogo += Time.deltaTime / 6;
                tituloImage.color = new Color(tituloImage.color.r, tituloImage.color.g, tituloImage.color.b, opacidadLogo);
            }

            if(opacidadLogo >= 1)
            {
                opacidadBotones += Time.deltaTime;

                restartButton.enabled = true;
                quitButton.enabled = true;

                restartImage.color = new Color(restartImage.color.r, restartImage.color.g, restartImage.color.b, opacidadBotones);
                quitImage.color = new Color(quitImage.color.r, quitImage.color.g, quitImage.color.b, opacidadBotones);

                restartText.color = new Color(restartText.color.r, restartText.color.g, restartText.color.b, opacidadBotones);
                quitText.color = new Color(quitText.color.r, quitText.color.g, quitText.color.b, opacidadBotones);
            }
        }
    }
}
