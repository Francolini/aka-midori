﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuitGame : MonoBehaviour
{
    public GameObject pauseMenu, cursor, rombos, player;

    private CursorHidder cursorScript;
    private Rombos bossMuerto;
    private PlayerHealth playerHealth;
    private PlayerScript playerScript;

    private void Start()
    {
        cursorScript = cursor.GetComponent<CursorHidder>();
        bossMuerto = rombos.GetComponent<Rombos>();
        playerHealth = player.GetComponent<PlayerHealth>();
        playerScript = player.GetComponent<PlayerScript>();
    }

    private void Update()
    {
        if (Input.GetButtonDown("Cancel") && !bossMuerto.bossMuerto && !playerHealth.stopPauseMenu)
        {
            PauseGame();
        }
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void PauseGame()
    {
        if (Time.timeScale == 0f)
        {
            Time.timeScale = 1f;
            pauseMenu.SetActive(false);
            cursorScript.HideCursor();
            playerScript.enabled = true;

        }
        else
        {
            Time.timeScale = 0f;
            pauseMenu.SetActive(true);
            cursorScript.ShowCursor();
            playerScript.enabled = false;
        }
    }

    public void PauseMenuDisabled()
    {
        if (Time.timeScale == 0f)
        {
            Time.timeScale = 1f;
            pauseMenu.SetActive(false);
            cursorScript.HideCursor();
        }
    }
}
