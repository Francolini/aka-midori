﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class HealthLight : MonoBehaviour
{
    private Light2D healthLight;

    private bool subirIntensidad;

    // Start is called before the first frame update
    void Start()
    {
        subirIntensidad = false;
        healthLight = GetComponent<Light2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (healthLight.intensity <= 2)
        {
            subirIntensidad = true;
        }

        if(healthLight.intensity >= 7)
        {
            subirIntensidad = false;
        }

        if (subirIntensidad)
        {
            healthLight.intensity += Time.deltaTime * 5;
        }
        else
        {
            healthLight.intensity -= Time.deltaTime * 5;
        }
    }
}
