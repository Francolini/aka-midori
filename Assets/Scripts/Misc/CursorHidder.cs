﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorHidder : MonoBehaviour
{    public void ShowCursor()
    {
        Cursor.visible = true;
    }

    public void HideCursor()
    {
        Cursor.visible = false;
    }
}
