﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platforms : MonoBehaviour
{
    private GameObject player;
    private Vector3 playerPos;
    private Collider2D platformCol;

    public GameObject platform;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        platformCol = GetComponent<Collider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        playerPos = player.transform.position;

        if(playerPos.y <= transform.position.y)
        {
            platformCol.isTrigger = true;
        }
        else
        {
            platformCol.isTrigger = false;
        }
    }
}
