﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrokenLight : MonoBehaviour
{
    public GameObject brokenLight;
    private float numRandom, contador;
    private bool activada;

    // Start is called before the first frame update
    void Start()
    {
        numRandom = Random.Range(0.3f, 3f);

        activada = false;
        
        contador = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if(contador <= numRandom)
        {
            contador += Time.deltaTime;
        }
        else
        {
            contador = 0;
            numRandom = Random.Range(0.1f, 0.5f);

            if (!activada)
            {
                brokenLight.SetActive(true);
                activada = true;
            }
            else
            {
                brokenLight.SetActive(false);
                activada = false;
            }
        }        
    }
}
