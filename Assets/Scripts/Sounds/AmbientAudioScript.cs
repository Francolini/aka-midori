﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmbientAudioScript : MonoBehaviour
{
    public GameObject musicController;

    private MusicController musicControllerScript;

    private void Start()
    {
        musicControllerScript = musicController.GetComponent<MusicController>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            musicControllerScript.ambientMusicActivated = true;
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            musicControllerScript.ambientMusicActivated = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            musicControllerScript.ambientMusicActivated = false;
            musicControllerScript.battleMusicActivated = false;
        }
    }
}
