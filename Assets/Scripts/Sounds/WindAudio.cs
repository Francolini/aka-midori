﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindAudio : MonoBehaviour
{
    public GameObject musicController;

    private MusicController musicControllerScript;

    private void Start()
    {
        musicControllerScript = musicController.GetComponent<MusicController>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            musicControllerScript.windBlowActivated = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if(collision.CompareTag("Player"))
        {
            musicControllerScript.windBlowActivated = false;
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            musicControllerScript.windBlowActivated = true;
            musicControllerScript.ambientMusicActivated = false;
        }
    }
}
