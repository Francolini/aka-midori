﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundDroneActivator : MonoBehaviour
{

    private AudioSource audioSound;

    void Start()
    {
        audioSound = GetComponent<AudioSource>(); 
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            audioSound.Play();
        } 
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            audioSound.Stop();
        }
    }
}
