﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossBattleScript : MonoBehaviour
{
    public GameObject musicController;

    private MusicController musicControllerScript;

    private void Start()
    {
        musicControllerScript = musicController.GetComponent<MusicController>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            musicControllerScript.bossBattleMusicActivated = true;
            musicControllerScript.stopCounting = false;
        }     
    }
}
