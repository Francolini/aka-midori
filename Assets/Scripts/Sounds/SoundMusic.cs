﻿using UnityEngine;
using System.Collections;

public class SoundMusic : MonoBehaviour
{
	public GameObject soundPaso01GO, soundPaso02GO, soundBossLegGO, soundPisadaBossGO, soundAttack01GO, soundAttack02GO, soundThrowHookGO, soundHookedGO, soundGetHookGO, soundNormalAttackBossGO, soundDashGO, soundHealGO,
		soundPlayerDeadGO, soundPlayerDamagedGO, soundBossSpecialAttack01GO, soundBossStandByGO, soundBossDamagedGO, soundBossDeadGO, soundBossChargingGO, soundAttack03GO, impactProjectileGO;

	private AudioSource soundPaso01, soundPaso02, soundBossLeg, soundPisadaBoss, soundAttack01, soundAttack02, soundThrowHook, soundHooked, soundGetHook, soundNormalAttackBoss, soundDash, soundHeal, soundPlayerDead, soundPlayerDamaged, soundBossSpecialAttack01,
		soundBossStandBy, soundBossDamaged, soundBossDead, soundBossCharging, soundAttack03, impactProjectile;

	public bool noSound, boolSoundPaso01, boolSoundPaso02, boolSoundBossLeg, boolSoundPisadaBoss, boolSoundAttack01, boolSoundAttack02, boolSoundThrowHook, boolSoundHooked, boolSoundGetHook, boolSoundNormalAttackBoss, boolSoundDash, boolSoundHeal, boolPlayerDead, 
		boolSoundPlayerDamaged, boolSoundBossSpecialAttack01, boolSoundBossStandBy, boolBossDamaged, boolSoundBossDead, boolSoundBossCharging, boolSoundAttack03, boolImpactProjectile;

	private bool OnnOff;

	// Use this for initialization
	void Start()
	{
		OnnOff = true;

		soundPaso01 = soundPaso01GO.GetComponent<AudioSource>();
		soundPaso02 = soundPaso02GO.GetComponent<AudioSource>();
		soundBossLeg = soundBossLegGO.GetComponent<AudioSource>();
		soundPisadaBoss = soundPisadaBossGO.GetComponent<AudioSource>();
		soundAttack01 = soundAttack01GO.GetComponent<AudioSource>();
		soundAttack02 = soundAttack02GO.GetComponent<AudioSource>();
		soundThrowHook = soundThrowHookGO.GetComponent<AudioSource>();
		soundHooked = soundHookedGO.GetComponent<AudioSource>();
		soundGetHook = soundGetHookGO.GetComponent<AudioSource>();
		soundNormalAttackBoss = soundNormalAttackBossGO.GetComponent<AudioSource>();
		soundDash = soundDashGO.GetComponent<AudioSource>();
		soundHeal = soundHealGO.GetComponent<AudioSource>();
		soundPlayerDead = soundPlayerDeadGO.GetComponent<AudioSource>();
		soundPlayerDamaged = soundPlayerDamagedGO.GetComponent<AudioSource>();
		soundBossSpecialAttack01 = soundBossSpecialAttack01GO.GetComponent<AudioSource>();
		soundBossStandBy = soundBossStandByGO.GetComponent<AudioSource>();
        soundBossDamaged = soundBossDamagedGO.GetComponent<AudioSource>();
		soundBossDead = soundBossDeadGO.GetComponent<AudioSource>();
		soundBossCharging = soundBossChargingGO.GetComponent<AudioSource>();
		soundAttack03 = soundAttack03GO.GetComponent<AudioSource>();
		impactProjectile = impactProjectileGO.GetComponent<AudioSource>();
	}

	// Update is called once per frame
	void FixedUpdate()
	{
		if (OnnOff)
		{
            if (boolSoundPaso01)
            {
				soundPaso01.Play();
				boolSoundPaso01 = false;
			}

			if (boolSoundPaso02)
			{
				soundPaso02.Play();
				boolSoundPaso02 = false;
			}

			if (boolSoundAttack01)
            {
				soundAttack01.Play();
				boolSoundAttack01 = false;
			}

            if (boolSoundAttack02)
			{
				soundAttack02.Play();
				boolSoundAttack02 = false;
			}

            if (boolSoundThrowHook)
            {
				soundThrowHook.Play();
				boolSoundThrowHook = false;
			}

			if (boolSoundHooked)
			{
				soundHooked.Play();
				boolSoundHooked = false;
			}

			if (boolSoundGetHook)
			{
				soundGetHook.Play();
				boolSoundGetHook = false;
			}

            if (boolSoundBossLeg)
            {
				soundBossLeg.Play();
				boolSoundBossLeg = false;
			}

            if (boolSoundPisadaBoss)
            {
				soundPisadaBoss.Play();
				boolSoundPisadaBoss = false;
			}
			
            if (boolSoundNormalAttackBoss)
            {
				soundNormalAttackBoss.Play();
				boolSoundNormalAttackBoss = false;
			}

            if (boolSoundDash)
            {
				soundDash.Play();
				boolSoundDash = false;
			}

            if (boolSoundHeal)
            {
				soundHeal.Play();
				boolSoundHeal = false;
            }

            if (boolPlayerDead)
            {
				soundPlayerDead.Play();
				boolPlayerDead = false;
            }

            if (boolSoundBossSpecialAttack01)
            {
				soundBossSpecialAttack01.Play();
				boolSoundBossSpecialAttack01 = false;
			}

            if (boolSoundPlayerDamaged)
            {
				soundPlayerDamaged.Play();
				boolSoundPlayerDamaged = false;
            }

            if (boolSoundBossStandBy)
            {
				soundBossStandBy.Play();
				boolSoundBossStandBy = false;
			}

            if (boolBossDamaged)
            {
				soundBossDamaged.Play();
				boolBossDamaged = false;
            }

            if (boolSoundBossDead)
            {
				soundBossDead.Play();
				boolSoundBossDead = false;
            }

			if (boolSoundBossCharging)
			{
				soundBossCharging.Play();
				boolSoundBossCharging = false;
			}

            if (boolSoundAttack03)
            {
				soundAttack03.Play();
				boolSoundAttack03 = false;
            }

            if (boolImpactProjectile)
            {
				impactProjectile.Play();
				boolImpactProjectile = false;
			}
		}
	}
}