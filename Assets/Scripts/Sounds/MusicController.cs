﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicController : MonoBehaviour
{
    public GameObject ambient01, ambient02, bossBattle, windBlowObject;
    public AudioClip bossBattle01, bossBattle02;

    public bool ambientMusicActivated, battleMusicActivated, bossBattleMusicActivated, stopCounting, windBlowActivated;
    public float volumeBoss;

    private AudioSource ambientAS01, ambientAS02, bossBattleAS, windBlow;
    private bool gameStarted;

    private float volumeAmbient, volumeBattle, contador, volumeWind;

    // Start is called before the first frame update
    void Start()
    {
        battleMusicActivated = false;
        ambientMusicActivated = false;
        windBlowActivated = false;

        volumeBattle = 0;
        volumeAmbient = 0;
        volumeBoss = 0;
        volumeWind = 0;

        stopCounting = true;
        gameStarted = false;

        ambientAS01 = ambient01.GetComponent<AudioSource>();
        ambientAS02 = ambient02.GetComponent<AudioSource>();

        bossBattleAS = bossBattle.GetComponent<AudioSource>();

        windBlow = windBlowObject.GetComponent<AudioSource>();

        contador = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (ambientMusicActivated && gameStarted)
        {
            if (!ambientAS01.isPlaying && !ambientAS02.isPlaying)
            {
                ambientAS01.Play();
                ambientAS02.Play();
            }

            if (volumeAmbient <= .6f)
            {
                volumeAmbient += Time.deltaTime / 4;
                ambientAS01.volume = volumeAmbient;
            }
        }
        else
        {
            if (volumeAmbient >= 0)
            {
                volumeAmbient -= Time.deltaTime / 4;
                ambientAS01.volume = volumeAmbient;
            }
            else
            {
                ambientAS01.Stop();
                ambientAS02.Stop();
            }
        }

        if (battleMusicActivated)
        {
            if (volumeBattle <= .4f)
            {
                volumeBattle += Time.deltaTime / 2;
                ambientAS02.volume = volumeBattle;
            }
        }
        else
        {
            if (volumeBattle >= 0)
            {
                volumeBattle -= Time.deltaTime / 10;
                ambientAS02.volume = volumeBattle;
            }
        }

        if (bossBattleMusicActivated)
        {
            volumeBoss = bossBattleAS.volume;

            if (contador == 0 && !bossBattleAS.isPlaying)
            {
                bossBattleAS.volume = .7f;
                bossBattleAS.clip = bossBattle01;
                bossBattleAS.Play();

                contador++;
            }


            if (contador == 1 && !bossBattleAS.isPlaying)
            {
                bossBattleAS.loop = true;
                bossBattleAS.volume = .5f;
                bossBattleAS.clip = bossBattle02;
                bossBattleAS.Play();

                contador++;
            }
        }
        else
        {
            bossBattleAS.loop = false;
            volumeBoss -= Time.deltaTime / 2;
            bossBattleAS.volume = volumeBoss;
            contador = 0;

            if (volumeBoss <= 0)
            {
                volumeBoss = 0;
                bossBattleAS.Stop();
            }
        }

        if (windBlowActivated)
        {
            if (!windBlow.isPlaying)
            {
                windBlow.Play();
            }

            if (volumeWind <= .6f)
            {
                volumeWind += Time.deltaTime / 2;
                windBlow.volume = volumeWind;
            }
        }
        else
        {
            if (volumeWind >= 0f)
            {
                volumeWind -= Time.deltaTime / 2;
                windBlow.volume = volumeWind;
            }
            else
            {
                windBlow.Stop();
            }
        }
    }

    public void PlayAmbientMusic()
    {
        gameStarted = true;
    }

    public void StopBossBattleMusic()
    {
        bossBattleMusicActivated = false;
    }
}
