﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : MonoBehaviour
{
    private GameObject player;
    private Rigidbody2D rigiPlayer;
    private Rigidbody2D rigi;
    private Animator animator;
    private Animator animPlayer;
    private AudioSource[] specialAttack2Audio;
    private SoundMusic sm;
    private Rombos rombosScript;
    private PlayerScript playerScript;

    private float direction;
    private int randomAttack, oleadas;
    private bool doingNormalAttack;

    public GameObject energyWave;
    public GameObject energyWavesPoint;
    public GameObject bossSpawner;
    public GameObject rombos;

    public float speed, attackRange;
    public bool stopBoss;

    public GameObject cursorHidder;
    private CursorHidder cursorHidderScript;

    private void Start()
    {
        sm = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<SoundMusic>();

        transform.position = bossSpawner.transform.position;

        player = GameObject.FindGameObjectWithTag("Player");
        rigi = GetComponent<Rigidbody2D>();
        rigiPlayer = player.GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        animPlayer = player.GetComponent<Animator>();
        specialAttack2Audio = GetComponents<AudioSource>();

        speed = 7;
        oleadas = 0;

        doingNormalAttack = false;
        stopBoss = false;

        attackRange = 50.0f;

        rombosScript = rombos.GetComponent<Rombos>();

        playerScript = player.GetComponent<PlayerScript>();

        cursorHidderScript = cursorHidder.GetComponent<CursorHidder>();
    }

    private void Update()
    {
        if (!stopBoss)
        {
            if (player.transform.position.x < transform.position.x)
            {
                direction = -1;
                transform.localScale = new Vector2(1, transform.localScale.y);
            }
            else
            {
                direction = 1;
                transform.localScale = new Vector2(-1, transform.localScale.y);
            }

            if (!doingNormalAttack)
            {
                if (Vector2.Distance(player.transform.position, rigi.transform.position) > attackRange)
                {
                    rigi.velocity = new Vector2(direction * speed, rigi.velocity.y);
                    animator.SetBool("Idle", false);
                }
                else
                {
                    rigi.velocity = new Vector2(0, rigi.velocity.y);
                    animator.SetBool("Idle", true);
                }
            }
            else
            {
                if (Vector2.Distance(new Vector2(player.transform.position.x, 0), new Vector2(rigi.transform.position.x, 0)) > attackRange)
                {
                    rigi.velocity = new Vector2(direction * speed, rigi.velocity.y);
                    animator.SetBool("Idle", false);
                }
                else
                {
                    rigi.velocity = new Vector2(0, rigi.velocity.y);

                    attackRange = 50.0f;

                    animator.SetBool("Idle", true);
                    animator.SetTrigger("Attack");

                    doingNormalAttack = false;
                }
            }
        }
        else
        {
           rigi.velocity = new Vector2(0, 0);
        }
    }

    public void DoSpecialAttack(int n)
    {
        oleadas = n;      
    } 

    public void DiscountWaves()
    {
        oleadas--;

        animator.SetFloat("FinishedWaves", oleadas);
    }

    public void ThrowEnergyWave()
    {
        GameObject wave = Instantiate(energyWave, energyWavesPoint.transform.position, energyWavesPoint.transform.rotation);
        Rigidbody2D rigiWave = wave.GetComponent<Rigidbody2D>();

        rigiWave.velocity = (player.transform.position - energyWavesPoint.transform.position).normalized * 40;
    }

    public void ChangeRange()
    {
        doingNormalAttack = true;

        attackRange = 7.0f;
    }

    public int ThinkAttack()
    {
        randomAttack = Random.Range(0, 2);

        return randomAttack;
    }

    public void StartGeiserSound()
    {
        specialAttack2Audio[0].enabled = true;
        specialAttack2Audio[0].Play();
    }

    public void StartExplosion()
    {
        specialAttack2Audio[1].enabled = true;
        specialAttack2Audio[1].Play();
    }

    public void OnRestart()
    {
        transform.position = bossSpawner.transform.position;

        player = GameObject.FindGameObjectWithTag("Player");
        rigi = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        specialAttack2Audio = GetComponents<AudioSource>();

        speed = 7;
        oleadas = 0;

        doingNormalAttack = false;
        stopBoss = false;

        attackRange = 50.0f;
    }

    public void StopBoss(bool b)
    {
        stopBoss = b;
    }

    public void SoundLeg()
    {
        sm.boolSoundBossLeg = true;
    }

    public void SoundStep()
    {
        sm.boolSoundPisadaBoss = true;
    }

    public void SoundNormalAttack()
    {
        sm.boolSoundNormalAttackBoss = true;
    }

    public void BossMuerto()
    {
        rombosScript.bossMuerto = true;
        rigiPlayer.velocity = new Vector2(0, rigiPlayer.velocity.y);
        animPlayer.enabled = false;
        playerScript.enabled = false;
        cursorHidderScript.ShowCursor();
    }

    public void SoundSpecialAttack01()
    {
        sm.boolSoundBossSpecialAttack01 = true;
    }

    public void SoundStandBy()
    {
        sm.boolSoundBossStandBy = true;
    }

    public void BossDamaged()
    {
        sm.boolBossDamaged = true;
    }

    public void BossDying()
    {
        sm.boolSoundBossDead = true;
    }

    public void BossCharging()
    {
        sm.boolSoundBossCharging = true;
    }
}