﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossKiller : MonoBehaviour
{
    private Animator anim;
    private Boss boss;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponentInParent<Animator>();
        boss = GetComponentInParent<Boss>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("PlayerHit"))
        {
            anim.SetTrigger("Dead");
            boss.stopBoss = true;
        }
    }
}
