﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss_Recharge : StateMachineBehaviour
{
    GameObject deadZone;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        deadZone = GameObject.FindGameObjectWithTag("DeadZone");

        deadZone.SetActive(false);
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        deadZone.SetActive(true);
    }
}
