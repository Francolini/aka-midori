﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss_Idle : StateMachineBehaviour
{
    private GameObject deadZone;

    private Animator animDZ;
    private SpriteRenderer srDZ;
    private Collider2D cDZ;
    private Boss boss;

    private int attack = 0;
    private float timer = 3f;

    public bool attacking = true, randomAttackBool = false, attackBool = false;

    //OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        deadZone = GameObject.FindGameObjectWithTag("DeadZone");

        animDZ = deadZone.GetComponent<Animator>();
        srDZ = deadZone.GetComponent<SpriteRenderer>();
        cDZ = deadZone.GetComponent<Collider2D>();

        animDZ.enabled = true;
        srDZ.enabled = true;
        cDZ.enabled = true;

        boss = animator.GetComponent<Boss>();
        timer = Random.Range(0.5f, 4);

        attacking = false;
    }

    //OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (!attacking)
        {
            if(timer > 0)
            {
                timer -= Time.deltaTime;
            }
            else
            {
                attack = boss.ThinkAttack();

                switch (attack)
                {
                    case 0:

                        timer = 2.0f;
                        animator.SetTrigger("RandomAttack");
                        randomAttackBool = true;
                        attacking = true;

                        break;

                    case 1:

                        boss.ChangeRange();

                        timer = 2.0f;
                        attackBool = true;
                        attacking = true;

                        break;
                }
            }
        }
    }

    //OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (randomAttackBool)
        {
            animator.ResetTrigger("RandomAttack");
        }

        if (attackBool)
        {
            animator.ResetTrigger("Attack");
        }
    }
}