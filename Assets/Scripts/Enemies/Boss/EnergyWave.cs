﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyWave : MonoBehaviour
{
    private GameObject player;
    private PlayerHealth playerHealth;
    private SoundMusic sm;

    public ParticleSystem particles;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");

        playerHealth = player.GetComponent<PlayerHealth>();
        sm = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<SoundMusic>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("PlayerHurt"))
        {
            Destroy(gameObject);

            playerHealth.AddDamage(75);
            GameObject.Instantiate(particles, transform.position, transform.rotation);

            sm.boolImpactProjectile = true;
        }

        if (collision.CompareTag("Ground"))
        {
            Destroy(gameObject);
            GameObject.Instantiate(particles, transform.position, transform.rotation);

            sm.boolImpactProjectile = true;
        }

        if(playerHealth.playerDead)
        {
            Destroy(gameObject);
        }
    }
}