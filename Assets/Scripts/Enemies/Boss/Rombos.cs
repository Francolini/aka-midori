﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rombos : MonoBehaviour
{
    public GameObject romboRojo, romboVerde, ui;

    public bool bossMuerto;

    private SpriteRenderer rR, rV;

    private float contadorR, contadorV;
    private UIManager uiManager;

    // Start is called before the first frame update
    void Start()
    {
        uiManager = ui.GetComponent<UIManager>();

        rR = romboRojo.GetComponent<SpriteRenderer>();
        rV = romboVerde.GetComponent<SpriteRenderer>();

        contadorR = 1;
        contadorV = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (bossMuerto)
        {
            if (contadorR >= 0)
            {
                contadorR -= Time.deltaTime / 2;
                rR.color = new Color(rR.color.r, rR.color.g, rR.color.b, contadorR);
            }

            if (contadorV <=1)
            {
                contadorV += Time.deltaTime / 2;
                rV.color = new Color(rV.color.r, rV.color.g, rV.color.b, contadorV);
            }
            else
            {
                uiManager.gameFinished = true;
            }
        }
        else
        {
            contadorR = 1;
            contadorV = 0;

            rR.color.a.Equals(contadorR);
            rV.color.a.Equals(contadorV);
        }
    }
}
