﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceToPlayer : MonoBehaviour
{
    GameObject target;
    Vector2 targetToAim;

    float angle;
    // Start is called before the first frame update
    void Start()
    {

        target = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {

        targetToAim = target.transform.position - transform.position;

        angle = Mathf.Atan2(targetToAim.y, targetToAim.x) * Mathf.Rad2Deg;

        transform.rotation = Quaternion.Euler(Vector3.forward * angle);

    }
}
