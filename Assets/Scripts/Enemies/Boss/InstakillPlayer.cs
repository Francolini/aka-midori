﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstakillPlayer : MonoBehaviour
{
    private GameObject player;
    private PlayerHealth playerHealth;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");

        playerHealth = player.GetComponent<PlayerHealth>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("PlayerHurt"))
        {
            playerHealth.AddDamage(100);
        }
    }
}
