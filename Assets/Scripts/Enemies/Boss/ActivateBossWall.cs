﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateBossWall : MonoBehaviour
{
    public GameObject boss;

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            boss.SetActive(true);
        }
    }
}
