﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss_RandomAttack : StateMachineBehaviour
{
    private Boss  boss;

    private int randomAttack;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        boss = animator.GetComponent<Boss>();
        
        randomAttack = Random.Range(0, 5);
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if(randomAttack <= 1)
        {
            animator.SetTrigger("SpecialAttack2");
        }
        else
        {
            animator.SetFloat("FinishedWaves", randomAttack);
            boss.DoSpecialAttack(randomAttack);

            animator.SetTrigger("SpecialAttack");
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        
    }
}
