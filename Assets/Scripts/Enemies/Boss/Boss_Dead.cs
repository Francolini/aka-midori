﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss_Dead : StateMachineBehaviour
{
    GameObject deadZone;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        deadZone = GameObject.FindGameObjectWithTag("DeadZone");

        if(deadZone != null)
        {
            deadZone.SetActive(false);
        }
    }
}
