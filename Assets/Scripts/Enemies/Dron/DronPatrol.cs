﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DronPatrol : MonoBehaviour
{
    private Vector2 startPosition;
    private Vector2 limitVR, limitVL;
    private Rigidbody2D rigi;
    private GameObject player;
    private DistanceJoint2D distance;

    private bool attackPlayer, returnPosition, hasArrivedToPoint, searchPlayer, playerEscapes;
    private float stopSpeed;

    //public GameObject spawnPoint;

    public float limitR, limitL, speed;
    public bool patrolMode, beginPatrol;
    public int direction;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        distance = GetComponent<DistanceJoint2D>();
        distance.enabled = false;
        rigi = GetComponent<Rigidbody2D>();
        startPosition = transform.position;

        limitVR = new Vector2(startPosition.x + limitR, 0);
        limitVL = new Vector2(startPosition.x - limitL, 0);

        patrolMode = true;
        beginPatrol = true;
        attackPlayer = false;
        returnPosition = false;
        hasArrivedToPoint = true;
        searchPlayer = false;
        playerEscapes = true;

        stopSpeed = speed;
    }

    // Update is called once per frame
    void Update()
    {
        if (patrolMode)
        {
            attackPlayer = false;
            distance.enabled = false;
            
            if (hasArrivedToPoint)
            {
                beginPatrol = true;
                searchPlayer = false;
            }
            else
            {
                returnPosition = true;
                searchPlayer = false;
            }
        }
        else
        {
            returnPosition = true;
            playerEscapes = true;
            attackPlayer = true;
            hasArrivedToPoint = false;
            beginPatrol = false;

            FlipToTarget(player.transform.position);
        }   
    }

    void FixedUpdate()
    {
        if (beginPatrol)
        {
            StartCoroutine("DronePatroling");
        }

        if (attackPlayer)
        {
            if (searchPlayer)
            {
                distance.connectedAnchor = new Vector2(player.transform.position.x, player.transform.position.y);
                distance.enabled = true;

                if (transform.position.y - 5 < player.transform.position.y)
                {
                    rigi.velocity = new Vector2(0, 2);
                }
            }
            else
            {
                if (player.transform.position.x + 5 < transform.position.x || player.transform.position.x - 5 > transform.position.x || player.transform.position.y + 5 < transform.position.y || player.transform.position.y - 5 > transform.position.y)
                {
                    transform.position = Vector2.MoveTowards(transform.position, player.transform.position, 12 * Time.deltaTime);
                }
                else
                {
                    searchPlayer = true;
                }
            }
        }

        if(attackPlayer)
        {
            StopCoroutine("DroneReturn");
        }

        if (!hasArrivedToPoint && !attackPlayer)
        {
            if (playerEscapes)
            {
                StartCoroutine("DroneReturn");
            }

            if (!playerEscapes)
            {
                FlipToTarget(startPosition);

                transform.position = Vector2.MoveTowards(transform.position, startPosition, 7 * Time.deltaTime);

                if (transform.position.Equals(startPosition) && returnPosition)
                {
                    rigi.MovePosition(startPosition);
                    hasArrivedToPoint = true;
                    returnPosition = false;
                }
            }
        }
    }

    private void ChangeDirection()
    {
        direction *= -1;
    }

    private void FlipToTarget(Vector2 target)
    {
        if(transform.position.x > target.x)
        {
            transform.localScale = new Vector2(-1, transform.localScale.y);
            direction = -1;
        }
        else
        {
            transform.localScale = new Vector2(1, transform.localScale.y);
            direction = 1;
        }
    }

    private void FlipToDirection(float dir)
    {
        transform.localScale = new Vector2(dir, transform.localScale.y);
    }

    private void MovePatrol()
    {
        transform.Translate(new Vector2(direction * speed, 0) * Time.deltaTime, Space.World);
    }

    private IEnumerator DronePatroling()
    {
        MovePatrol();
        FlipToDirection(direction);

        if (transform.position.x >= limitVR.x)
        {
            transform.position = new Vector2(transform.position.x - 0.1f, transform.position.y);

            speed = 0;
            yield return new WaitForSeconds(1.0f);

            speed = stopSpeed;
            ChangeDirection();
        }

        if (transform.position.x <= limitVL.x)
        {
            transform.position = new Vector2(transform.position.x + 0.1f, transform.position.y);

            speed = 0;
            yield return new WaitForSeconds(1.0f);

            speed = stopSpeed;
            ChangeDirection();
        }
    }

    private IEnumerator DroneReturn()
    {
        if (playerEscapes)
        {
            rigi.velocity = new Vector2(3f * direction, 0);

            yield return new WaitForSeconds(2f);

            rigi.velocity = new Vector2(0, 0);
            playerEscapes = false;
        }
    }
}