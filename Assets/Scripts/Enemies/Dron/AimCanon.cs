﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimCanon : MonoBehaviour
{ 
    public GameObject target;
    public GameObject drone;
    public GameObject bulletPrefab;
    public GameObject bulletSoundPrefab;
    public GameObject anchor;
    public GameObject bulletPoint;

    public float speed, aimRotationSpeed;

    private DronPatrol dronePatrolScript;
    private Vector3 targetToAim;
    private Animator anim;

    private bool letsAim;
    private float angle;

    // Start is called before the first frame update
    void Start()
    {
        dronePatrolScript = drone.GetComponent<DronPatrol>();
        anim = GetComponent<Animator>();

        letsAim = false;

        aimRotationSpeed = 1.0f;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = anchor.transform.position;
        
        if (dronePatrolScript.patrolMode)
        {
            letsAim = false;
        }
        else
        {
            letsAim = true;
        }
    }

    private void FixedUpdate()
    {
        if (letsAim)
        {
            target = GameObject.FindGameObjectWithTag("Player");
            
            targetToAim = target.transform.position - transform.position;

            angle = Mathf.Atan2(targetToAim.y, targetToAim.x) * Mathf.Rad2Deg;
        
            transform.rotation = Quaternion.Euler(Vector3.forward * angle);

            StartCoroutine("ShootTarget");

        } else
        {
            StopCoroutine("ShootTarget");

            if(transform.rotation.z != -90)
            {
                transform.rotation = Quaternion.Euler(new Vector3(0, 0, -90));
            }
        }
    }

    private void LateUpdate()
    {
        if (letsAim)
        {
            anim.SetBool("Attacking", true);
        }
        else
        {
            anim.SetBool("Attacking", false);
        }
    }

    private IEnumerator ShootTarget()
    {
        yield return new WaitForSeconds(1.3f);
            
        StartCoroutine("Rafaga");

        StopCoroutine("ShootTarget");
    }

    private IEnumerator Rafaga()
    {
        float random = Random.Range(0, 4);

        for (float i = 0; i <= random; i++)
        {
            yield return new WaitForSeconds(0.12f);

            if (bulletPrefab != null)
            {
                GameObject bullet = Instantiate(bulletPrefab, bulletPoint.transform.position, transform.rotation);
                GameObject bulletSound = Instantiate(bulletSoundPrefab, bulletPoint.transform.position, transform.rotation);

                Rigidbody2D rigiBullet = bullet.GetComponent<Rigidbody2D>();
                DronBullet bulletScript = bullet.GetComponent<DronBullet>();

                rigiBullet.velocity = (target.transform.position - transform.position).normalized * 30;

                bulletScript.AimAndShoot();
            }
        }

        StopCoroutine("Rafaga");
    }
}