﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroneKiller : MonoBehaviour
{
    private void OnDisable()
    {
        GameObject[] droneList = GameObject.FindGameObjectsWithTag("Drone");

        int i = 0;
        int length = droneList.Length;

        while(i < length)
        {
            Destroy(droneList[i]);

            i++;
        }
    }
}