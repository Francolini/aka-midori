﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DronBullet : MonoBehaviour
{
    public GameObject player;
    private PlayerHealth playerHealth;

    public void AimAndShoot()
    {
        StartCoroutine("Shoot");
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("PlayerHurt"))
        {
            Destroy(gameObject);

            playerHealth = collision.gameObject.GetComponentInParent<PlayerHealth>();

            playerHealth.AddDamage(15.0f);
        }

        if (collision.CompareTag("Ground"))
        {
            Destroy(gameObject);
        }
    }

    public IEnumerator Shoot()
    {
        yield return new WaitForSeconds(1.5f);

        Destroy(gameObject);
    }
}