﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroneDeath : MonoBehaviour
{
    private float timeLeft;

    private void Start()
    {
        timeLeft = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (timeLeft < 3)
        {
            timeLeft += Time.deltaTime;
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
