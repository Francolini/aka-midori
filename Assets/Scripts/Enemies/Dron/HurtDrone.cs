﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HurtDrone : MonoBehaviour
{
    public GameObject drone, droneDeathPrefab;

    private GameObject musicController;
    private MusicController musicControllerScript;

    private void Start()
    {
        musicController = GameObject.FindGameObjectWithTag("MusicController");
        musicControllerScript = musicController.GetComponent<MusicController>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("PlayerHit"))
        {
            Destroy(drone);
            GameObject droneDeath = Instantiate(droneDeathPrefab, transform.position, transform.rotation);

            musicControllerScript.battleMusicActivated = false;
        }
    }
}