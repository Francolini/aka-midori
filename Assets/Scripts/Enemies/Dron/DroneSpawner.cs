﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroneSpawner : MonoBehaviour
{

    public GameObject dronePrefab;

    private void OnEnable()
    {
        Instantiate(dronePrefab, transform.position, transform.rotation);
    }
}
