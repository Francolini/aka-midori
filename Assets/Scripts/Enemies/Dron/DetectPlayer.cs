﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectPlayer : MonoBehaviour
{
    public GameObject drone;

    private bool playerDetected;

    private GameObject musicController;
    private DronPatrol droneScript;
    private MusicController musicControllerScript;

    // Start is called before the first frame update
    void Start()
    {
        musicController = GameObject.FindGameObjectWithTag("MusicController");
        musicControllerScript = musicController.GetComponent<MusicController>();

        droneScript = drone.GetComponent<DronPatrol>();

        playerDetected = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (playerDetected)
        {
            droneScript.patrolMode = false;
        }
        else
        {
            droneScript.patrolMode = true;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            musicControllerScript.battleMusicActivated = true;
            playerDetected = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            musicControllerScript.battleMusicActivated = false;
            playerDetected = false;
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            musicControllerScript.battleMusicActivated = true;
            playerDetected = true;
        }
    }
}