﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{
    public Transform groundChecker;
    public SpriteRenderer hookTip;
    public LayerMask groundLayer;
    public Vector2 colliderSize;
    public GameObject hookPoint;
    public GameObject startHook, endHook, hookSprite;
    public DistanceJoint2D hook;
    public LineRenderer rope;
    public SoundMusic sm;

    public float speed, jumpForce, colliderAngle, dashForce, dashTimer, secondAttackTimer;
    public bool canHook, hookingAnim, hookPhys, stopHook, hookConnected, stopMoving, isDashing, letsHook, bugFixer, isInGround;
    public int jumpCounter;

    private Animator anim;
    private Rigidbody2D rigi;
    private Collider2D playerCol;

    private bool letsAttack, isAttacking, letsDash, letsJump, canSecondAttack, isCounting, letsAttack2, attack2stopped, animAnotherJump;
    private float xDirection, dashingTime, auxSpeed, secondAttackTime;
    private int bugSoundFix, bugSoundFix2;

    // Start is called before the first frame update
    void Start()
    {
        hookPoint = null;
        startHook = GameObject.FindGameObjectWithTag("StartHook");
        endHook = GameObject.FindGameObjectWithTag("EndHook");

        anim = GetComponent<Animator>();
        rigi = GetComponent<Rigidbody2D>();
        hook = GetComponent<DistanceJoint2D>();
        rope = GetComponent<LineRenderer>();
        playerCol = GetComponent<CapsuleCollider2D>();
        hookTip = hookSprite.GetComponent<SpriteRenderer>();

        isAttacking = false;
        hookingAnim = false;
        isInGround = true;
        letsAttack = false;
        letsDash = false;
        isDashing = false;
        letsJump = false;
        canHook = false;
        letsHook = false;
        hookPhys = false;
        stopHook = false;
        hookConnected = false;
        stopMoving = false;
        canSecondAttack = false;
        isCounting = false;
        letsAttack2 = false;
        attack2stopped = false;
        bugFixer = false;

        jumpCounter = 0;
        bugSoundFix = 0;
        bugSoundFix2 = 0;

        xDirection = 0.0f;
        auxSpeed = 0.0f;
        secondAttackTime = secondAttackTimer;
        dashingTime = dashTimer;
    }

    // Update is called once per frame
    void Update()
    {
        if (!isDashing)
        {
            xDirection = Input.GetAxisRaw("Horizontal");
        }

        if (xDirection < 0 && transform.localScale.x > 0)
        {
            Flip();

        } else if(xDirection > 0 && transform.localScale.x < 0)
        {
            Flip();
        }

        if (Input.GetButtonDown("Jump") && jumpCounter <= 1 && !isDashing && !isAttacking)
        {
            letsJump = true;

            if (jumpCounter == 1 && !isInGround)
            {
                animAnotherJump = true;
            }

            jumpCounter++;
        }

        if (secondAttackTime >= secondAttackTimer)
        {
            canSecondAttack = false;
            isCounting = false;
        }

        if (Input.GetButtonDown("Fire1") && !isDashing && secondAttackTime < secondAttackTimer && isCounting && isInGround && !letsAttack2)
        {
            stopMoving = true;
            isCounting = false;
            letsAttack2 = true;
            secondAttackTime = 0.0f;
        }

        if (canSecondAttack)
        {
            secondAttackTime += Time.deltaTime;
        }

        if (Input.GetButtonDown("Fire1") && !isDashing && !isCounting && !canSecondAttack && !letsAttack2 || !isInGround && Input.GetButtonDown("Fire1") && !isDashing && !letsAttack2)
        {
            letsAttack = true;
            secondAttackTime = 0.0f;
            canSecondAttack = true;
            isCounting = true;
        }

        if (dashingTime < dashTimer)
        {
            dashingTime += Time.deltaTime;
        }

        if (Input.GetButtonDown("Fire3") && !isAttacking && rigi.velocity.x != 0 && !isDashing)
        {
            if(dashingTime >= dashTimer)
            {
                letsDash = true;
                dashingTime = 0.0f;
            }
        }

        if (canHook && Input.GetButtonDown("Fire2") && !bugFixer)
        {
            if (hookPoint.transform.position.x > transform.position.x && transform.localScale.x == -1)
            {
                Flip();
            }

            if (hookPoint.transform.position.x < transform.position.x && transform.localScale.x == 1)
            {
                Flip();
            }

            bugFixer = true;
            letsHook = true;
            stopMoving = true;
        }

        if (hookPhys && !isInGround)
        {
            playerCol.isTrigger = true;
        }
        else
        {
            playerCol.isTrigger = false;
        }
    }

    //Update fisicas
    private void FixedUpdate()
    {
        //isInGround = Physics2D.OverlapCapsule(groundChecker.position, colliderSize, colliderDirection, colliderAngle, groundLayer);

        if (!isAttacking)
        {
            auxSpeed = speed;

            if (isDashing)
            {
                auxSpeed = speed * 3.8f;
            }

            rigi.velocity = new Vector2(xDirection * auxSpeed, rigi.velocity.y);
        }
        else
        {
            rigi.velocity = new Vector2(0, rigi.velocity.y);
        }

        if (letsJump)
        {
            rigi.velocity = new Vector2(rigi.velocity.x, 0);

            rigi.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);

            letsJump = false;
        }

        if (stopMoving)
        {
            rigi.velocity = new Vector2(0, 0);
        }

        if (hookPoint != null)
        {
            if (hookPhys)
            {
                hook.connectedAnchor = hookPoint.transform.position;

                rope.enabled = true;
                hookTip.enabled = true;

                rope.SetPosition(0, startHook.transform.position);
                rope.SetPosition(1, endHook.transform.position);

                if (!stopHook)
                {
                    endHook.transform.position = Vector2.MoveTowards(endHook.transform.position, hookPoint.transform.position, 40 * Time.deltaTime);
                }

                if (endHook.transform.position == hookPoint.transform.position)
                {
                    if (bugSoundFix == 0)
                    {
                        sm.boolSoundHooked = true;
                    }

                    bugSoundFix++;

                    hookConnected = true;
                }

                if (hookConnected)
                {
                    endHook.transform.position = hookPoint.transform.position;
                    hookingAnim = true;
                    stopHook = true;
                    hook.enabled = true;
                }
                
                if(Vector2.Distance(startHook.transform.position, endHook.transform.position) <= 1.7 && hookConnected)
                {
                    rope.SetPosition(0, Vector2.zero);
                    rope.SetPosition(1, Vector2.zero);
                    hook.enabled = false;
                    rope.enabled = false;
                    hookingAnim = false;
                    hookTip.enabled = false;
                    stopMoving = false;
                    hookPhys = false;
                    stopHook = false;
                    endHook.transform.position = startHook.transform.position;
                    hookConnected = false;
                    canHook = false; 
                    bugFixer = false;
                    hookPhys = false;
                    playerCol.isTrigger = false;
                }
            }
        }

        if (!hookPhys)
        {
            endHook.transform.position = startHook.transform.position;
            bugSoundFix = 0;
            bugSoundFix2 = 0;
        }
    }

    //Update de animaciones
    private void LateUpdate()
    {
        if (xDirection == 0 && !isAttacking)
        {
            anim.SetBool("Idle", true);
        }
        else
        {
            anim.SetBool("Idle", false);
        }

        anim.SetBool("isInGround", isInGround);
        anim.SetFloat("Velocity", rigi.velocity.y);

        if (letsAttack && !letsAttack2)
        {
            if (isInGround)
            {
                anim.SetTrigger("Attack");
                letsAttack = false;
            }

            if (!isInGround)
            {
                anim.SetTrigger("AirAttack");
                letsAttack = false;
            }
        }

        if (letsAttack2)
        {
            anim.SetBool("Attack2", true);
        }

        if(attack2stopped)
        {
            anim.SetBool("Attack2", false);
            attack2stopped = false;
        }

        if (letsDash)
        {
            anim.SetTrigger("Dash");
            letsDash = false;
        }

        if (letsHook)
        {
            anim.SetTrigger("ThrowHook");
            letsHook = false;
        }

        if (hookingAnim && stopHook)
        {
            anim.SetBool("Hooking", true);
        }
        else
        {
            anim.SetBool("Hooking", false);
        }

        if (animAnotherJump)
        {
            anim.SetTrigger("JumpAgain");
            animAnotherJump = false;
        }
    }

    //Funciones personalizadas
    public void AttackingON()
    {
        isAttacking = true;
    }
    public void AttackingOFF()
    {
        isAttacking = false;
    }

    public void DashingON()
    {
        isDashing = true;
    }
    public void DashingOFF()
    {
        isDashing = false;
        sm.boolSoundDash = true;
    }
    public void ThrowHook()
    {
        sm.boolSoundThrowHook = true;
        hookPhys = true;
    }
    public void StopAttack2()
    {
        stopMoving = false;
        attack2stopped = true;
        letsAttack2 = false;
    }

    void Flip()
    {
        endHook.transform.localScale = new Vector3(transform.localScale.x * -1, transform.localScale.y, transform.localScale.z);
        transform.localScale = new Vector3(transform.localScale.x * -1, transform.localScale.y, transform.localScale.z);
    }

    //sounds activator

    void Attack01Sound()
    {
        sm.boolSoundAttack01 = true;
    }

    void Attack02Sound()
    {
        sm.boolSoundAttack02 = true;
    }
    void Attack02Sound02()
    {
        sm.boolSoundAttack03 = true;
    }

    void StepSound01()
    {
        sm.boolSoundPaso01 = true;
    }

    void StepSound02()
    {
        sm.boolSoundPaso02 = true;
    }

    void GetHookSound()
    {
        if (bugSoundFix2 == 0)
        {
            sm.boolSoundGetHook = true;
        }

        bugSoundFix2++;
    }

    void SoundDash()
    {
        sm.boolSoundDash = true;
    }

    void SoundDead()
    {
        sm.boolPlayerDead = true;
    }
}