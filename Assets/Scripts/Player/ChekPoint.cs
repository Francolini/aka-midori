﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChekPoint : MonoBehaviour
{
    private SoundMusic sm;

    public GameObject respawnPlayer, player, healthLight, soundMusic, bocadill;

    private PlayerHealth playerHealth;
    private Animator animBocadill;

    private void Start()
    {
        sm = soundMusic.GetComponent<SoundMusic>();
        playerHealth = player.GetComponent<PlayerHealth>();
        animBocadill = bocadill.GetComponent<Animator>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            sm.boolSoundHeal = true;
            respawnPlayer.transform.position = transform.position;
            playerHealth.health = 100;
            healthLight.SetActive(true);
            animBocadill.SetBool("ShowBocadill", true);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            healthLight.SetActive(false);
            animBocadill.SetBool("ShowBocadill", false);
        }
    }
}
