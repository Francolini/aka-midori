﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundChecker : MonoBehaviour
{
    public GameObject player;

    private PlayerScript playerScript;

    void Start()
    {
        playerScript = player.GetComponent<PlayerScript>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Ground") || collision.CompareTag("HookPost"))
        {
            playerScript.isInGround = true;
            playerScript.jumpCounter = 0;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Ground") || collision.CompareTag("HookPost"))
        {
            playerScript.isInGround = false;
        }
    }
}
