﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour
{
    private float widthRT;

    private RectTransform widthHealthBar;
    private Animator playerAnimator;
    private SpriteRenderer playerHurted;
    private MusicController musicController;
    private SpriteRenderer endHookSR;
    private Collider2D colPlayer;

    public bool invencible, playerDead, stopPauseMenu;
    public float health;

    public PlayerScript playerScript;
    public Rigidbody2D rigi;
    public GameObject healthBar;
    public GameObject retryMenu;
    public GameObject pointsOfRespawnDrones;
    public GameObject respawnPoint;
    public GameObject bossAudioGO;
    public GameObject soundController;
    public GameObject endHook;

    public GameObject cursorHidder;
    private CursorHidder cursorHidderScript;
    private SoundMusic soundControllerScript;

    void Start()
    {
        transform.position = respawnPoint.transform.position;

        playerAnimator = GetComponent<Animator>();
        widthHealthBar = healthBar.GetComponent<RectTransform>();
        playerHurted = GetComponent<SpriteRenderer>();
        playerScript = GetComponent<PlayerScript>();
        rigi = GetComponent<Rigidbody2D>();
        musicController = GameObject.FindGameObjectWithTag("MusicController").GetComponent<MusicController>();
        endHookSR = endHook.GetComponent<SpriteRenderer>();
        soundControllerScript = soundController.GetComponent<SoundMusic>();
        colPlayer = GetComponent<CapsuleCollider2D>();

        invencible = false;
        playerDead = false;
        stopPauseMenu = false;

        widthRT = 298;
        health = 100;

        cursorHidderScript = cursorHidder.GetComponent<CursorHidder>();
    }
    void Update()
    {
        if (health <= 0)
        {
            health = 100;
            rigi.velocity = new Vector2(0, 0);
            StopAllCoroutines();

            playerHurted.color = Color.white;

            healthBar.SetActive(false);

            invencible = true;
            playerDead = true;
            stopPauseMenu = true;

            playerScript.stopMoving = false;
            playerScript.enabled = false;
            playerScript.stopHook = false;
            playerScript.hookPhys = false;
            playerScript.isDashing = false;
            playerScript.rope.enabled = false;
            playerScript.canHook = false;
            playerScript.hookPoint = null;
            playerScript.hook.enabled = false;
            playerScript.hookConnected = false;
            playerScript.bugFixer = false;
            endHookSR.enabled = false;
            colPlayer.isTrigger = false;
        }
        else
        {
            widthHealthBar.sizeDelta = new Vector2(health * widthRT / 100, 50);
        }
    }

    private void LateUpdate()
    {
        if (playerDead)
        {
            playerAnimator.SetTrigger("Dead");
            playerDead = false;
        }
    }

    public void AddDamage(float damage)
    {
        if (!invencible)
        {
            soundControllerScript.boolSoundPlayerDamaged = true;

            invencible = true;
            StartCoroutine("HurtAnimation");

            health -= damage;
        }
    }

    public void onDeath()
    {
        musicController.StopBossBattleMusic();

        musicController.battleMusicActivated = false;
        musicController.ambientMusicActivated = false;

        pointsOfRespawnDrones.SetActive(false);

        retryMenu.SetActive(true);

        cursorHidderScript.ShowCursor();
    }

    public void onRestart()
    {
        stopPauseMenu = false;

        health = 100;
        transform.position = respawnPoint.transform.position;

        playerAnimator.SetTrigger("Restart");

        invencible = false;

        healthBar.SetActive(true);

        playerScript.enabled = true;

        pointsOfRespawnDrones.SetActive(true);
        playerDead = false;

        musicController.battleMusicActivated = false;
        musicController.ambientMusicActivated = true;
    }

    private IEnumerator HurtAnimation()
    {
        playerHurted.color = Color.red;

        yield return new WaitForSeconds(0.4f);

        playerHurted.color = Color.white;

        invencible = false;

        StopCoroutine("HurtAnimation");
    }
}