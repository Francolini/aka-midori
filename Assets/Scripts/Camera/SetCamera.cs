﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetCamera : MonoBehaviour
{

    public GameObject playerCamera, bossCamera;

    private Cinemachine.CinemachineVirtualCamera pCam, bCam;

    private bool playerInside;

    // Start is called before the first frame update
    void Start()
    {
        playerInside = false;
        pCam = playerCamera.GetComponent<Cinemachine.CinemachineVirtualCamera>();
        bCam = bossCamera.GetComponent<Cinemachine.CinemachineVirtualCamera>();
    }

    // Update is called once per frame
    void Update()
    {

        if (playerInside)
        {
            bCam.enabled = true;
            pCam.enabled = false;
        } else
        {
            bCam.enabled = false;
            pCam.enabled = true;
        }
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Player"))
        {
            playerInside = true;
        }
    }

    private void OnTriggerExit2D(Collider2D col)
    {
        if (col.CompareTag("Player"))
        {
            playerInside = false;
        }
    }
}
