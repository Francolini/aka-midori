﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleMountainCam : MonoBehaviour
{

    public Cinemachine.CinemachineVirtualCamera virtualCamera;

    private bool zoomIn, zoomOut;

    private void Start()
    {
        zoomIn = false;
        zoomOut = false;
    }

    private void FixedUpdate()
    {
        if (virtualCamera.m_Lens.OrthographicSize <= 18 && zoomOut)
        {
            virtualCamera.m_Lens.OrthographicSize += Time.deltaTime * 2;
        }

        if (virtualCamera.m_Lens.OrthographicSize >= 14 && zoomIn)
        {
            virtualCamera.m_Lens.OrthographicSize -= Time.deltaTime * 2;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        zoomOut = true;
        zoomIn = false;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        zoomOut = false;
        zoomIn = true;
    }
}
