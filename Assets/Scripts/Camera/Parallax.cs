﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour
{
    [SerializeField] private Vector2 moveMultiplier;

    private Transform cam;
    private Vector3 lateCameraPosition;

    // Start is called before the first frame update
    void Start()
    {
        cam = Camera.main.transform;
        lateCameraPosition = cam.position;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 movement = cam.position - lateCameraPosition;
        transform.position += new Vector3(movement.x * moveMultiplier.x, movement.y * moveMultiplier.y);

        lateCameraPosition = cam.position;
    }
}